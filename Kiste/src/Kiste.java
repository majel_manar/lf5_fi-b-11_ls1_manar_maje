
public class Kiste {
	private int hoehe;
	private int breite;
	private int tiefe;
	private String farbe;

	
	
	public Kiste(int hoehe, int breite, int tiefe, String farbe) {
		this.hoehe = hoehe;
		this.breite = breite;
		this.tiefe = tiefe;
		this.farbe = farbe;
	}

	public double getVolumen() {
		return  this.hoehe * this.breite * this.tiefe;
	}
	
	public String getFarbe(String farbe) {
		return farbe;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Kiste Kiste1 = new Kiste(1,1,1,"blau");
		System.out.println(Kiste1.getVolumen());
		System.out.println(Kiste1.getFarbe(Kiste1.farbe));
		
		Kiste kiste2 = new Kiste(2,2,2,"gr�n");
		System.out.println(kiste2.getVolumen());
		System.out.println(kiste2.getFarbe(kiste2.farbe));
		
		Kiste kiste3 = new Kiste(3,3,3,"rot");
		System.out.println(kiste3.getVolumen());
		System.out.println(kiste3.getFarbe(kiste3.farbe));
		
		
		
		
	}

}
