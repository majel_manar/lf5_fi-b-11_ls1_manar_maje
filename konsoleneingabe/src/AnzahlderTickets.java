import java.util.Scanner;
public class AnzahlderTickets {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner Tickets = new Scanner(System.in);
		
		
		
		System.out.print("Ticketpreis (Euro): ");
		float Preis = Tickets.nextFloat();//Ticketpreis kann Gleitkommavariable sein,in Beispiel ist 2,5.
		System.out.print("Anzahl der Tickets: ");
		int Anzahl = Tickets.nextInt();//Anzahl der Tickets muss ganzzahlige Variable sein.
		System.out.print("Noch zu zahlen: ");
		System.out.printf("%.2f" ,(Preis*Anzahl));//ich habe printf benutzt um ,00 auszugeben,und die Rechnung ist die Anzahl der Tickets mal der Preis.
  	    System.out.println(" Euro");
		
		
		
		Tickets.close();

}}
