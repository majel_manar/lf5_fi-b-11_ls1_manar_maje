import java.util.Scanner;

class letzteversionfahrkartenautomat{

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag; 
		double eingezahlterGesamtbetrag;
		int tickets;
       
	   	System.out.print("\nZu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
       
       	tickets = fahrkartenbestellungErfassen();
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tickets);
        fahrkartenAusgeben();
        r�ckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag, tickets);
	    tastatur.close();
	 
	}	
	
	public static int fahrkartenbestellungErfassen() {
	    
		Scanner tastatur = new Scanner(System.in);
		
       	System.out.println("Bitte geben Sie die Anzahl der Tickets ein");
       	int ticketmenge = 1;
       	int tickets = tastatur.nextInt();
       
   		if (0<tickets &&  tickets<=10) {
   			ticketmenge = tickets;
   		}
   		else {
   			System.out.println("falsch");
   			//return 1;
   		}	
   		return ticketmenge;
   		
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag, int tickets) {
		
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.00;
	    
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag * tickets)
	    {
	    System.out.printf("Noch zu zahlen: " + "%.2f Euro\n", (zuZahlenderBetrag*tickets - eingezahlterGesamtbetrag));
	    System.out.print("Eingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
	    double eingeworfeneM�nze = tastatur.nextDouble();
	    eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneM�nze;
	    }
	    
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
	  System.out.println("\nFahrschein wird ausgegeben");
	  for (int i = 0; i < 8; i++)
	  {
	    System.out.print("=");
	     try {
		    Thread.sleep(250);
		    } catch (InterruptedException e) {
			 // TODO Auto-generated catch block
				e.printStackTrace();
			 }
	       }
	       System.out.println("\n\n");	
	}
	
	public static void r�ckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag, int tickets) {
		
		Scanner tastatur = new Scanner(System.in);

		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * tickets;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro\n", r�ckgabebetrag);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }
	       tastatur.close();
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	    }	
}
