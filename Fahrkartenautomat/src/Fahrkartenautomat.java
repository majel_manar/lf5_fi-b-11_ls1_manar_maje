import java.util.Scanner;

class Fahrkartenautomat
{
    
    private static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args)
    {
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag = 0.0;
        double r�ckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        r�ckgabebetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(r�ckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");

    }

    private static double fahrkartenbestellungErfassen()
    {
        System.out.print("Ticketpreis (Euro): ");
        double ticketpreis = tastatur.nextDouble(); 
        System.out.print("Anzahl der Tickets: ");
        int ticketAnzahl = tastatur.nextInt(); 
        return ticketpreis * ticketAnzahl;
    }

   
    private static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlen)
    {
        double eingeworfeneM�nze = 0;

        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlen - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfeneM�nze = tastatur.nextDouble();

            if (eingeworfeneM�nze > 2.0 || eingeworfeneM�nze < 0.05)
            {
                System.out.println("Ung�ltige M�nze!");
                continue;
            }
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }

        return eingezahlterGesamtbetrag - zuZahlen;
    }

   
    private static void fahrkartenAusgeben()
    {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 16; i++)
        {
            System.out.print("=");
            warte(100);
        }
        System.out.println("\nFertig!\n\n");
        warte(500);
    }

    private static void warte(int millisekunde)
    {
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            System.out.println("\nAusgabe wurde abgebrochen!");
            e.printStackTrace();
        }
    }

    
    private static void rueckgeldAusgeben(double r�ckgabebetrag)
    {
        if(r�ckgabebetrag > 0.0)
        {
            System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO \n", r�ckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            
            r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, 2, "EURO");
            r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, 1, "EURO");
            r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, 50, "CENT");
            r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, 20, "CENT");
            r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag, 10, "CENT");
            r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,  5, "CENT");

        }
    }

    
    private static double muenzeAusgeben(double rueckgabebetrag, int betragAbsolut, String einheit)
    {
        switch(einheit) 
        {
            case "EURO":
                while(rueckgabebetrag >= betragAbsolut)
                {
                    System.out.println(betragAbsolut + " " + einheit);
                    rueckgabebetrag -= betragAbsolut;
                }
                return rueckgabebetrag;

            case "CENT":
            default:
                double betragInCent = ((double)betragAbsolut/100);
                while(rueckgabebetrag + 0.000001 >= betragInCent) 
                {
                    System.out.println(betragAbsolut + " " + einheit);
                    rueckgabebetrag -= betragInCent;
                }
                return rueckgabebetrag;
        }
    }
}